#!/bin/bash

if [ "$1" == "set" ]
   then
     sed -e 's/#AutomaticLogin/AutomaticLogin/'g -i /etc/gdm/custom.conf
     systemctl restart gdm
   elif [ "$1" == "unset" ]
       then
          sed -e 's/^AutomaticLogin/#AutomaticLogin/'g -i /etc/gdm/custom.conf
          systemctl restart gdm
fi

