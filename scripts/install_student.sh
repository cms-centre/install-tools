#!/bin/bash

POST="/data/install-tools"

if ! rpm -q --quiet chromium-browser  ; then 
yum install -y chromium-browser
fi


# this configure desktop with autologin and local user for SLC6

if [ ! -d "/home/student" ]
 then


    # make user
    groupadd -g 600 student
    useradd -G 600 -g 600 -u 600  -p "\$6\$U0fJiz40\$VOzV3thv6LKOOs6.CQRlka.q0kPU2gxuSNpIU0PXXWQBWRSeA/WF13xq9L20FMHYKcZ/5irEZZoDp6k0kwkho0" student  

    #echo "xset s off" >> /home/student/.bash_profile
    #echo "xset -dpms" >> /home/student/.bash_profile


    mkdir -p /home/student/.config/autostart
    
    cat <<EOF >/home/student/.config/autostart/firefox.desktop
[Desktop Entry]
Type=Application
Exec=/usr/bin/firefox -display :0.0 https://www.i2u2.org/elab/cms/cima/index.php  
Hidden=false
X-GNOME-Autostart-enabled=true
Name=firefox
Comment=firefox
EOF

    mkdir -p /home/student/.gconf/apps/gnome-screensaver
    cp -f $POST/scripts/student_homedir/.gconf/apps/gnome-screensaver/* /home/student/.gconf/apps/gnome-screensaver/ 

    cp -f $POST/scripts/student_homedir/.k5login /home/student/

    mkdir -p /home/student/Desktop



    cp -rp $POST/scripts/student_homedir/Desktop/* /home/student/Desktop/
    chown -R student:student /home/student/

    # disable lock screen
    gconftool-2 --direct \
      --config-source xml:readwrite:/etc/gconf/gconf.xml.mandatory \
      --type bool \
      --set /apps/gnome-screensaver/lock_enabled false

    #prepare autologin
    sed -e 's/\[daemon\]//'g -i /etc/gdm/custom.conf
    cat <<EOF >>/etc/gdm/custom.conf
[daemon]
#AutomaticLoginEnable=true
#AutomaticLogin=student
EOF
   
fi

