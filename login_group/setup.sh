#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
user="None"
if [ $1 ]
  then
    user=$1
fi

if [ "`grep CentOS /etc/redhat-release`" == "" ]
   then
      distro=SLC`cat /etc/redhat-release |sed -e 's/^.* \([0-9]\).[0-9].*$/\1/'`
   else
      distro=CC`cat /etc/redhat-release |sed -e 's/^.* \([0-9]\).[0-9].*$/\1/'`
fi

if [ "$user" = "all" ]
   then
     if [ "$distro" = "SLC5" ]
       then
    	  echo "process for ldap ..."
          /bin/rm -f /etc/ldap.conf
          cd $dir/ldapusers_tree
          tar  --owner=root --group=bin -cf  - . |(cd / ; tar  xvfBp -)
          /sbin/chkconfig --level 345  nscd on
          /etc/init.d/nscd restart
          cd -
	
	
     elif [ "$distro" = "SLC6" ] 
        then
	    /bin/rm -f /etc/ldap.conf
            /bin/rm -rf /etc/openldap  
            echo "NETWORKWAIT=1" >> /etc/sysconfig/network
	    yum install -y nss-pam-ldapd CERN-CA-certs
	    sed -e 's/passwd:.*/passwd:     files ldap/g' -i /etc/nsswitch.conf
	    sed -e 's/group:.*/group:       files ldap/g' -i /etc/nsswitch.conf
	    /bin/cp -f $dir/nslcd.conf /etc/.
            /sbin/chkconfig --level 345 nslcd on
            /etc/init.d/nslcd restart 

     elif  [ "$distro" = "CC7" ]
        then
            #http://linux.web.cern.ch/linux/docs/account-mgmt.shtml
            /bin/cp -f $dir/sssd.conf /etc/sssd/.
            authconfig --enablesssd --enablesssdauth --update
            systemctl enable sssd
            systemctl stop sssd
            systemctl start sssd

     fi     




   elif [ $user != "None" ]
      then
         echo "process for AFS login group"
         sed -e 's/ldap//g' -i /etc/nsswitch.conf
	 if [ -f "/etc/init.d/nslcd" ]
            then
	       /etc/init.d/nslcd stop
	       yum remove -y nss-pam-ldapd
	 fi
	    
	 /usr/sbin/addusercern $user
         sh $dir/greeter.sh 
         rpm -ivh $dir/prod_rpm/$distro/`uname -i`/cmscc_logingroup-[0-9]*.`uname -i`.rpm
	 
fi

  
