#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
type=$1
host=`uname -n|cut -f1 -d'.'`

if [ $type == "desktop" ] ;then
    if [ -f $dir/xorg.conf ]; then
         /bin/cp -f $dir/xorg.conf /etc/X11/xorg.conf
    fi

else
    if  [ $type == "displayonly" ] ;then
        if [ -f $dir/xorg.conf.displayonly.$host ] ;then
             /bin/cp -f $dir/xorg.conf.displayonly.$host /etc/X11/xorg.conf
        else
           if  [ -f $dir/xorg.conf.displayonly ];then
             /bin/cp -f $dir/xorg.conf.displayonly /etc/X11/xorg.conf
           fi
       fi
   fi
fi

