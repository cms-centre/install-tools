#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
type=$1
if [ $type == "desktop" ]
  then
    if [ -f $dir/xorg.conf ]
       then
         /bin/cp -f $dir/xorg.conf /etc/X11/xorg.conf
    fi
    
elif [ $type == "displayonly" ]
  then
    if [ -f $dir/xorg.conf.displayonly ]
       then
         /bin/cp -f $dir/xorg.conf.displayonly /etc/X11/xorg.conf
    fi
else
fi
