#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`

if [ $1 == "2" ]
then

/bin/cp -f $dir/.xsession /home/ccconsole/
chown  ccconsole:zh /home/ccconsole/.xsession

yum install -y xdotool
yum install -y openbox


mkdir -p /home/ccconsole/.config/openbox
cp /etc/xdg/openbox/rc.xml /home/ccconsole/.config/openbox
sed -e 's/<\/applications>//' -i /home/ccconsole/.config/openbox/rc.xml
sed -e 's/<\/openbox_config>//' -i /home/ccconsole/.config/openbox/rc.xml
cat << Eof >> /home/ccconsole/.config/openbox/rc.xml
<application class="Firefox">
      <desktop>all</desktop>
      <maximized>yes</maximized>
      <decor>no</decor>
    </application>
</applications>
</openbox_config>
Eof

chown -R ccconsole:zh /home/ccconsole/.config/openbox
mv /usr/share/xsessions/openbox.desktop  /usr/share/xsessions/openbox.desktop.disable 
fi
