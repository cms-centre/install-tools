#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
user=ccconsole
conf=/etc/gdm/

#enable GDM autologin
cp -f $dir/custom.conf $conf/.

# install xwd for screenshot ( failed with import ... )
yum install -y xorg-x11-apps

# install without windowmanager

rm -f /usr/share/xsessions/*
cat << Eof_session >/usr/share/xsessions/ccconsole.desktop
[Desktop Entry]
Name=User script
Comment=This session runs ~/.xsession 
Exec=/home/ccconsole/.xsession
Eof_session


# install x11vnc 
sed -e 's/enabled=0/enabled=1/g' -i /etc/yum.repos.d/dag.repo
yum install -y x11vnc vnc x11vnc-javaviewers
cp -f $dir/index.vnc /usr/share/x11vnc/classes/.

yum install -y xterm ImageMagick
yum install -y openbox
yum install -y xdotool

/bin/rm -f /usr/share/xsessions/openbox.desktop

# add user ccconsole 
groupadd -g 1399 zh
useradd -G zh -g zh -u 500 ccconsole -p \$1\$GjD2oHhd\$ls0xV6Of04HvAD4KarSvs1

echo "graymond@CERN.CH" > /home/ccconsole/.k5login
echo "ccweb@CERN.CH" >> /home/ccconsole/.k5login
chown ccconsole:zh /home/ccconsole/.k5login
cp $dir/restart_display /root/.
cp -rp $dir/.vnc ~ccconsole/.
chown -R ccconsole:zh ~ccconsole/.vnc
chmod 600 ~ccconsole/.vnc/passwd

cp -rp $dir/.xsession ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.xsession
chmod +x /home/ccconsole/.xsession

cp -rp $dir/screenshot ~ccconsole/.
chown ccconsole:zh /home/ccconsole/screenshot
chmod +x /home/ccconsole/screenshot

cp -rp $dir/send_snaps.pl  ~ccconsole/.
chown ccconsole:zh /home/ccconsole/send_snaps.pl 
chmod +x /home/ccconsole/send_snaps.pl

cp -f $dir/.dmrc ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.dmrc

cp -f $dir/check_modif ~ccconsole/.
chown ccconsole:zh /home/ccconsole/check_modif
chmod +x /home/ccconsole/check_modif

cp -f $dir/.aumixrc ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.aumixrc

cp -rp $dir/.mplayer ~ccconsole/.
chown -R ccconsole:zh ~ccconsole/.mplayer

cp -rp $dir/start_firefox ~ccconsole/.
chown ccconsole:zh /home/ccconsole/start_firefox
chmod +x /home/ccconsole/start_firefox
cp -rp $dir/.mozilla ~ccconsole/.
chown -R ccconsole:zh ~ccconsole/.mozilla



# firewalld replace iptables
firewall-cmd --permanent --new-zone=vnczone
firewall-cmd --permanent --zone=vnczone --add-source=137.138.72.0/24 
firewall-cmd --permanent --zone=vnczone --add-source=137.138.178.235 
firewall-cmd --permanent --zone=vnczone --add-port=5800-5803/tcp
firewall-cmd --permanent --zone=vnczone  --add-port=5900-5903/tcp

systemctl restart firewalld

# access by ssh -C  -L 5910:localhost:5900 ccconsole@cmscc23
# then vncviewer localhost:0 ( :1)

# or directly : ssh -C  -L 5910:localhost:5900 ccconsole@cmscc23 vncviewer localhost:0 

# or javatool :
# cd /usr/share/vnc/classes ;java -Xms512m -Xmx1024m -cp ./vncviewer.jar vncviewer/VNCViewer cms06::5900 

# special configs for displays in building 40
if [ "`uname -n |sed -e 's/\(.*\)-.*$/\1/'`" == "cmsccb40" ]
  then
    sed -e 's/\&width/\&centre=CMS_Centre@CERN-B40\\\&width/g' -i ~ccconsole/start_firefox
    sed -e 's/1280/1900/g' -i ~ccconsole/start_firefox
    sed -e 's/1024/1080/g' -i ~ccconsole/start_firefox
    sed -e 's/6999/6998/g' -i  ~ccconsole/send_snaps.pl
fi
