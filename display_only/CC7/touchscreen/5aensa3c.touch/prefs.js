# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 * For more information, see http://www.mozilla.org/unix/customizing.html#prefs
 */

user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1287748896);
user_pref("app.update.lastUpdateTime.background-update-timer", 1287749375);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1287749205);
user_pref("app.update.lastUpdateTime.microsummary-generator-update-timer", 1287748860);
user_pref("app.update.lastUpdateTime.places-maintenance-timer", 1287748229);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1287748200);
user_pref("browser.download.manager.showWhenStarting", false);
user_pref("browser.fullscreen.autohide", false);
user_pref("browser.history_expire_days.mirror", 180);
user_pref("browser.migration.version", 1);
user_pref("browser.places.importBookmarksHTML", false);
user_pref("browser.places.smartBookmarksVersion", 2);
user_pref("browser.privatebrowsing.autostart", true);
user_pref("browser.rights.3.shown", true);
user_pref("browser.sessionstore.resume_from_crash", false);
user_pref("browser.sessionstore.resume_session_once", true);
user_pref("browser.startup.homepage", "http://www.cern.ch");
user_pref("browser.startup.homepage_override.mstone", "rv:1.9.2.11");
user_pref("extensions.enabledItems", "auntie12@hellokitty.com:0.1,{bfe3406c-6f31-4789-86d5-efa50e12c9eb}:3.4,{972ce4c6-7e08-4474-a285-3208198ce6fd}:3.6.11");
user_pref("extensions.lastAppVersion", "3.6.11");
user_pref("extensions.update.notifyUser", false);
user_pref("fullfullscreen.notabs", true);
user_pref("fullfullscreen.start", true);
user_pref("intl.charsetmenu.browser.cache", "ISO-8859-1, UTF-8");
user_pref("network.cookie.prefsMigrated", true);
user_pref("privacy.sanitize.migrateFx3Prefs", true);
user_pref("urlclassifier.keyupdatetime.https://sb-ssl.google.com/safebrowsing/newkey", 1290340054);
user_pref("xpinstall.whitelist.add", "");
user_pref("xpinstall.whitelist.add.36", "");
