
#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
chmod 600 ~ccconsole/.vnc/passwd 

cp -f $dir/.xsession ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.xsession
chmod +x /home/ccconsole/.xsession

cp -f $dir/iptables /etc/sysconfig/iptables
/sbin/service iptables restart

