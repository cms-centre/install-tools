
#!/bin/bash

dir=`echo $0 | sed -e 's/\(.*\)\/.*/\1/'`
user=ccconsole
conf=/etc/gdm/

#enable GDM autologin
cp -f $dir/custom.conf $conf/.

#set dual screen Clone mode
#sed -e 's/"Xinerama" "On"/"Xinerama" "Off"/' -i /etc/X11/xorg.conf
#sed -e 's/"Clone" "Off"/"Clone" "On"/' -i /etc/X11/xorg.conf


#allow TCP for remote X11 access ( in use with xhost + )
# sed -e 's/#DisallowTCP=true/DisallowTCP=flase/' -i $conf
# and rule for iptables -A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp --dport 6000 -j ACCEPT

# install x11vnc
sed -e 's/enabled=0/enabled=1/g' -i /etc/yum.repos.d/dag.repo
yum install -y x11vnc vnc


# add user ccconsole 
groupadd -g 1399 zh
useradd -G zh -g zh -u 500 ccconsole -p \$1\$GjD2oHhd\$ls0xV6Of04HvAD4KarSvs1

echo "graymond@CERN.CH" > /home/ccconsole/.k5login
echo "lucas@CERN.CH" >> /home/ccconsole/.k5login
echo "ccweb@CERN.CH" >> /home/ccconsole/.k5login
chown ccconsole:zh /home/ccconsole/.k5login
cp $dir/restart_display /root/.
cp -rp $dir/.vnc ~ccconsole/.
chown -R ccconsole:zh ~ccconsole/.vnc
chmod 600 ~ccconsole/.vnc/passwd

cp -rp $dir/touchscreen/.xsession ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.xsession
chmod +x /home/ccconsole/.xsession


cp -f $dir/.aumixrc ~ccconsole/.
chown ccconsole:zh /home/ccconsole/.aumixrc

cp -rp $dir/.mplayer ~ccconsole/.
chown -R ccconsole:zh ~ccconsole/.mplayer

cp -rp $dir/touchscreen/restart_firefox /root/.
chmod +x /root/restart_firefox

mkdir -p ~ccconsole/.mozilla/firefox
cp -rp $dir/touchscreen/5aensa3c.touch ~ccconsole/.mozilla/firefox/
cp -f $dir/touchscreen/profiles.ini ~ccconsole/.mozilla/firefox/
chown -R ccconsole:zh ~ccconsole/.mozilla

cp -f $dir/touchscreen/index.vnc /usr/share/x11vnc/classes/


grep -v REJECT /etc/sysconfig/iptables >/tmp/iptables
grep -v COMMIT /tmp/iptables >/etc/sysconfig/iptables
cat <<EOF >>/etc/sysconfig/iptables
-A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp -s 137.138.72.0/24 --dport 5800:5803 -j ACCEPT
-A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp -s 137.138.178.235 --dport 5800:5803 -j ACCEPT
-A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp -s 137.138.72.0/24 --dport 5900:5903 -j ACCEPT
-A RH-Firewall-1-INPUT -m state --state NEW -m tcp -p tcp -s 137.138.178.235 --dport 5900:5903 -j ACCEPT
-A RH-Firewall-1-INPUT -j REJECT --reject-with icmp-host-prohibited
COMMIT
EOF

/sbin/service iptables restart

# access by ssh -C  -L 5910:localhost:5900 ccconsole@cmscc23
# then vncviewer localhost:0 ( :1)

# or directly : ssh -C  -L 5910:localhost:5900 ccconsole@cmscc23 vncviewer localhost:0 

# or javatool :
# cd /usr/share/x11vnc/classes ;java -Xms512m -Xmx1024m -cp ./vncviewer.jar vncviewer/VNCViewer cms06::5900 

