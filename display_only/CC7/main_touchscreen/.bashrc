# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

if [ "`ps -ef|grep xvkbd|grep -v grep`" == "" ]
  then
   ./.xsession
fi
