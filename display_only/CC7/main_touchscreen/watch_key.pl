#!/usr/bin/perl

use POSIX;
use Socket;

# Forking into a new daemon...

my $pid = fork;
exit if $pid;
die "Couldn't fork: $!\n" unless defined($pid);
POSIX::setsid() || die "Cannot spawn new session id: $!\n";

my $port = shift || 5999;

my $proto = getprotobyname('tcp');
socket(Server, PF_INET, SOCK_STREAM, $proto) || die "Can't create socket: $!\n";
setsockopt(Server, SOL_SOCKET, SO_REUSEADDR, 1) || die "Can't setsockopt $!\n";
bind(Server, sockaddr_in($port, INADDR_ANY)) || die "Can't bind to socket: $!\n" ;
listen(Server,SOMAXCONN) || die "Can't listen to socket: $1";

# Infinite loop listening for client connections...

while ($paddr = accept(Client,Server))
  {
  my $key= <Client> ;
  my $res=`xvkbd -xsendevent -text "\\[F11]"` ;
 }


