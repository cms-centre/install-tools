var realfullscreen = {
    enteringFullscreen: function() {
		gBrowser.setStripVisibilityTo(window.fullScreen);
		document.getElementById('navigator-toolbox').setAttribute("hidden", !window.fullScreen);
    }
};

window.addEventListener('fullscreen', realfullscreen.enteringFullscreen, false);
