#!/usr/bin/perl


use IO::Socket;
my $snap = shift ;
my $server = IO::Socket::INET->new(
    PeerAddr => 'cmscentre.cern.ch',
    PeerPort => 6999,
    Proto    => 'tcp'
) or die "Can't create client socket: $!";

open FILE, "/tmp/$snap";
print $server "$snap\n";
while (<FILE>) {
    print $server $_;
}
close FILE;
close $server ;

